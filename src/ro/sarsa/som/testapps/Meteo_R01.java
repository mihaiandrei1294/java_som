package ro.sarsa.som.testapps;

import java.awt.Color;

import javax.swing.JFrame;

import ro.sarsa.som.SOM;
import ro.sarsa.som.topology.TorusSOMTopology;
import ro.sarsa.som.traindata.FileTrainData;
import ro.sarsa.som.traindata.FilterdTrainingData;
import ro.sarsa.som.traindata.NormalizedTrainingData;
import ro.sarsa.som.traindata.SOMTrainData;
import ro.sarsa.som.traindata.SimpleTrainData;
import ro.sarsa.som.umatrix.LabelApparenceProvider;
import ro.sarsa.som.umatrix.UMatrixPanel;

public class Meteo_R01 {
	public static void main(String[] args) {

			
		int row = 50;
		int col = 50;
		
		
		int ftrsToRemove[] = new int[] {};

			
		SOMTrainData unNormalizedtrData = new FileTrainData("7Proteine.txt");

		//SOMTrainData unNormalizedtrData = new FileTrainData("StructuralCoupling_RL.txt");
	
		unNormalizedtrData = new FilterdTrainingData(unNormalizedtrData,
				ftrsToRemove);

		SOMTrainData trData = new NormalizedTrainingData(unNormalizedtrData);
		// make a copy so we do not compute the input each time
		trData = new SimpleTrainData(trData);

		TorusSOMTopology topo = new TorusSOMTopology(row, col,
				trData.getDataDimension());

		topo.initRandom(0, 1);

		SOM som = new SOM(trData.getDataDimension(), topo);

		/*
		 * Latice2DSOMTopology topo = new Latice2DSOMTopology(row, col,
		 * trData.getDataDimension()); topo.initRandom(-0.5, 0.5); SOM som = new
		 * SOM(trData.getDataDimension(), topo);
		 */

		// umatrix
		UMatrixPanel umP = new UMatrixPanel(som, trData,
				new LabelApparenceProvider() {

					@Override
					public String getText(Object label) {
						return label.toString().substring(0,4);
						//ptr coupling
						//return label.toString().substring(1,label.toString().length());
					}

					@Override()
					public Color getColor(Object label) {
						System.out.println(label.toString());
						if (label.toString().charAt(4) == 'a') {
						//if (label.toString().charAt(0) == '1') {
							return Color.red;
						}
						if (label.toString().charAt(4) == 'b') 
							return Color.blue;
						//if (label.toString().charAt(0) == '2') {
						if (label.toString().charAt(4) == 'c') 
							return Color.green;
						if (label.toString().charAt(4) == 'd') 
							return Color.orange;
						if (label.toString().charAt(4) == 'e') 
							return Color.cyan;
						if (label.toString().charAt(4) == 'f') 
							return Color.yellow;
						if (label.toString().charAt(4) == 'g') 
							return Color.white;
						if (label.toString().charAt(4) == 'h') 
							return Color.pink;
						
						return Color.magenta;
						//return Color.red;
					}
				});
		JFrame jf = new JFrame("UMatrix");
		jf.setSize(400, 400);
		jf.getContentPane().add(umP);
		jf.setVisible(true);

		// antrenam
		// som.train2Phase(3000, trData, 0.7, topo.getMaxRadius() / 3, umP);
		som.train(200001, trData, 0.1, umP);
	}
}
