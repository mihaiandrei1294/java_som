package ro.sarsa.som.testapps;

import java.awt.Color;

import javax.swing.JFrame;

import ro.sarsa.som.SOM;
import ro.sarsa.som.topology.TorusSOMTopology;
import ro.sarsa.som.traindata.FileTrainData;
import ro.sarsa.som.traindata.FilterdTrainingData;
import ro.sarsa.som.traindata.NormalizedTrainingData;
import ro.sarsa.som.traindata.SOMTrainData;
import ro.sarsa.som.traindata.SimpleTrainData;
import ro.sarsa.som.umatrix.LabelApparenceProvider;
import ro.sarsa.som.umatrix.UMatrixPanel;

public class DefectPrediction {
	public static void main(String[] args) {

		int row = 50;// ar3, ar5 190-8, 40001, 0.7;
		int col = 50;// ar5  cu atr scoase de la 0 150-8, 70001, 0.7 - mai bine
                    //ar4   cu atr scoase 2, 27...  300-8, 70001, 0.7 - 11 erori
		//ar4   cu atr scoase 22, 1, ...  150-8, 70001, 0.7 - 11 erori
                 //scos 7->    13 erori
		         //scos 15->   12 erori
		         //scos 14, 16...   13 erori
		
		//int ftrsToRemove[] = new int[] {2, 3, 5};
		
		//int ftrsToRemove[] = new int[] {2, 3, 13, 6, 12, 19, 25, 26, 23, 24,
		//		28, 27 };// // + 8, 10, 0, 4, 21, 14, 16, features to Remove for Ar3:

		
		
		int ftrsToRemove[] = new int[] {9, 7, 8, 4, 10, 0, 20, 17, 18};// {6}; //  adaug 10 features to Remove for Ar4:

		//int ftrsToRemove[] = new int[] {0, 14, 16, 8, 3,     10, 11, 15, 20, 2, 22, 12, 23, 27,
		// 24, 26, 28};// {6}; //features to Remove for Ar5:

		//SOMTrainData unNormalizedtrData = new FileTrainData("all.txt");

		//SOMTrainData unNormalizedtrData = new FileTrainData("ar4_6Attrs.txt");

	    SOMTrainData unNormalizedtrData = new FileTrainData("ar5All.txt");

		unNormalizedtrData = new FilterdTrainingData(unNormalizedtrData,
				ftrsToRemove);

		// SOMTrainData unNormalizedtrData = new
		// FileTrainData("dateGenderWhite.txt");//92-7, 40000, 0.6

	    SOMTrainData trData = new NormalizedTrainingData(unNormalizedtrData);
		// make a copy so we do not compute the input each time
		trData = new SimpleTrainData(trData);

		TorusSOMTopology topo = new TorusSOMTopology(row, col,
				trData.getDataDimension());

		topo.initRandom(0, 1);

		SOM som = new SOM(trData.getDataDimension(), topo);

		/*
		 * Latice2DSOMTopology topo = new Latice2DSOMTopology(row, col,
		 * trData.getDataDimension()); topo.initRandom(-0.5, 0.5); SOM som = new
		 * SOM(trData.getDataDimension(), topo);
		 */

		// umatrix
		UMatrixPanel umP = new UMatrixPanel(som, trData,
				new LabelApparenceProvider() {

					@Override
					public String getText(Object label) {
						return label.toString().substring(0, 1);
					}

					@Override
					public Color getColor(Object label) {
						if (label.toString().charAt(0) == 'D') {
							return Color.red;
						}
						return Color.orange;
					}
				});
		JFrame jf = new JFrame("UMatrix");
		jf.setSize(400, 400);
		jf.getContentPane().add(umP);
		jf.setVisible(true);

		// antrenam
		// som.train2Phase(3000, trData, 0.7, topo.getMaxRadius() / 3, umP);
		som.train(200001, trData, 0.1, umP);
	}
}
