package ro.sarsa.som.testapps;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import javax.swing.JFrame;

import ro.sarsa.som.SOM;
import ro.sarsa.som.topology.TorusSOMTopology;
import ro.sarsa.som.traindata.FileTrainData;
import ro.sarsa.som.traindata.FilterdTrainingData;
import ro.sarsa.som.traindata.NormalizedTrainingData;
import ro.sarsa.som.traindata.SOMTrainData;
import ro.sarsa.som.traindata.SimpleTrainData;
import ro.sarsa.som.umatrix.DefaultLabelApparenceProvider;
import ro.sarsa.som.umatrix.LabelApparenceProvider;
import ro.sarsa.som.umatrix.UMatrixPanel;

public class GenderDetect {
	public static void main(String[] args) throws FileNotFoundException {

		int row =200;
		int col = 20;
	
		 int ftrsToRemove[] = {}; // features to Remove for "radius+ulna"

		 //SOMTrainData unNormalizedtrData = new FileTrainData("TerryExtins.txt");

		 //SOMTrainData unNormalizedtrData = new FileTrainData("radius+ulna.txt");

		 //SOMTrainData unNormalizedtrData = new FileTrainData("All_Trotter_184Instances_8ftrs_WithRace.txt");
		 
		SOMTrainData unNormalizedtrData = new FileTrainData("DateGenderWhite.txt");
		 
		 
		unNormalizedtrData = new FilterdTrainingData(unNormalizedtrData,
				ftrsToRemove);

		SOMTrainData trData = new NormalizedTrainingData(unNormalizedtrData);
		
		// make a copy so we do not compute the input each time

		
		TorusSOMTopology topo = new TorusSOMTopology(row, col,
				trData.getDataDimension());

		topo.initRandom(0, 1);

		SOM som = new SOM(trData.getDataDimension(), topo);

		/*
		 * Latice2DSOMTopology topo = new Latice2DSOMTopology(row, col,
		 * trData.getDataDimension()); topo.initRandom(-0.5, 0.5); SOM som = new
		 * SOM(trData.getDataDimension(), topo);
		 */

		//FileOutputStream fos = new FileOutputStream("AQE.csv", true);
		//final PrintStream ps = new PrintStream(fos);
	
		// umatrix
		UMatrixPanel umP = new UMatrixPanel(som, trData,
				new LabelApparenceProvider() {

					@Override
					public String getText(Object label) {
						return label.toString();//.substring(0, 1);
					}

					@Override
					public Color getColor(Object label) {
						if (label.toString().charAt(0) == 'M')
								return Color.BLUE;
						return Color.RED;
					}
				});
		JFrame jf = new JFrame("UMatrix");
		jf.setSize(500, 500);
		jf.getContentPane().add(umP);
		jf.setVisible(true);

		// antrenam
		// som.train2Phase(3000, trData, 0.7, topo.getMaxRadius() / 3, umP);
		som.train(40001, trData, 0.1, umP);
	}
}

