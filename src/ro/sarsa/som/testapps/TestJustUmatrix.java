package ro.sarsa.som.testapps;

import java.awt.Color;

import javax.swing.JFrame;

import ro.sarsa.som.SOM;
import ro.sarsa.som.topology.TorusSOMTopology;
import ro.sarsa.som.traindata.FileTrainData;
import ro.sarsa.som.traindata.FilterdTrainingData;
import ro.sarsa.som.traindata.NormalizedTrainingData;
import ro.sarsa.som.traindata.SOMTrainData;
import ro.sarsa.som.umatrix.LabelApparenceProvider;
import ro.sarsa.som.umatrix.UMatrixPanel;

public class TestJustUmatrix {
	public static void main(String[] args) {

		// RandomColorSetTrainingData rtrData = new
		// RandomColorSetTrainingData();
		// NormalizedTrainingData trData = new NormalizedTrainingData(rtrData);

		// SOMTrainData trData = new HeightTrainData("dateStatura.txt");
		// SOMTrainData unNormalizedtrData = new
		// FileTrainData("D:\\Anul 2\\MAP\\Java Eclipse\\eclipse\\workspace\\SelfOrganizingMap\\dateTest\\dateSergiu\\missingValuesMostCommonAttribute.txt");
		// SOMTrainData trData = new
		// FileTrainData("D:\\Anul 2\\MAP\\Java Eclipse\\eclipse\\workspace\\SelfOrganizingMap\\dateTest\\dateSergiu\\missingValueMostCommonAttribute.txt");
		// SOMTrainData unNormalizedtrData = new FileTrainData("ubb.txt");

		// SOMTrainData unNormalizedtrData = new
		// FileTrainData("ubbCompletateMediaUBB.txt");

		int row = 100;// 200  30;// 92;
		int col = 5;// 5  5;// 7;

		// SOMTrainData unNormalizedtrData = new FileTrainData(
		// "ubbCompletate0-5.txt");
		//

		// SOMTrainData unNormalizedtrData = new FileTrainData("ulna.txt");

		// SOMTrainData unNormalizedtrData = new
		// FileTrainData("ubbMicARtificial.txt");

		// SOMTrainData unNormalizedtrData = new FileTrainData(
		// "dateGenderNegro.txt");

		//int ftrsToRemove[] = {5, 17, 18}; // features to Remove for "radius+ulna"

		//int ftrsToRemove[] = {16,18,19}; // features to Remove for "Terry"

		int ftrsToRemove[] = {19}; 

		
		/*
		 * 1.Toate 2. Fara atributul 20 3. Fara 17, 19, 20 4. fara 1, 2, 13, 15,
		 * 17, 18, 19, 20
		 */

		 SOMTrainData unNormalizedtrData = new FilterdTrainingData(
				 new FileTrainData("TerryExtins.txt"), ftrsToRemove);

		//SOMTrainData unNormalizedtrData = new FilterdTrainingData(
		//		new FileTrainData("radius+ulna.txt"), ftrsToRemove);

		// int row = unNormalizedtrData.size();
		// int col = unNormalizedtrData.getDataDimension();

		// int row = trData.size();
		// int col = trData.getDataDimension();

		NormalizedTrainingData trData = new NormalizedTrainingData(
				unNormalizedtrData);

		TorusSOMTopology topo = new TorusSOMTopology(row, col,
				trData.getDataDimension());
		topo.initRandom(0, 1);
		// topo.initRandom(-0.5, 0.5);
		SOM som = new SOM(trData.getDataDimension(), topo);

		/*
		 * Latice2DSOMTopology topo = new Latice2DSOMTopology(row, col,
		 * trData.getDataDimension()); topo.initRandom(-0.5, 0.5); SOM som = new
		 * SOM(trData.getDataDimension(), topo);
		 */

		LabelApparenceProvider lblApp = new LabelApparenceProvider() {
			
			@Override
			public String getText(Object label) {				
				return label.toString().substring(0,1);
			}
			
			@Override
			public Color getColor(Object label) {
				if (label.toString().startsWith("M")){
					return Color.red;
				}
				return Color.yellow;
			}
		};
		// umatrix
		UMatrixPanel umP = new UMatrixPanel(som, trData,lblApp);
		JFrame jf = new JFrame("UMatrix");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setSize(500, 500);
		jf.getContentPane().add(umP);
		jf.setVisible(true);

		// antrenam
		// som.train2Phase(3000, trData, 0.7, topo.getMaxRadius() / 3, null);
		som.train(100001, trData, 0.6, null);
		umP.repaint();
	}
}
