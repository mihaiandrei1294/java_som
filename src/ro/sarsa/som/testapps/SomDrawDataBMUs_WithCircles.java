package ro.sarsa.som.testapps;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.Font;
import java.util.List;

import ro.sarsa.som.BMU;
import ro.sarsa.som.SOM;
import ro.sarsa.som.SOMNeuron;
import ro.sarsa.som.topology.SOMTopology;
import ro.sarsa.som.traindata.SOMTrainData;
import ro.sarsa.som.umatrix.LabelApparenceProvider;

public class SomDrawDataBMUs_WithCircles {

	public static void drawData(Graphics g, int w, int h, SOM som, SOMTrainData trD, LabelApparenceProvider lblAp) {
		System.out.println("Mda");

		// obtin lista de BMU
		List<BMU> bmus = som.getBMUS(trD);//, som, som.getDistance());
		System.out.println("Total bmu-s>" + bmus.size());
		g.setFont(new Font("default", Font.BOLD, 13));
		float thickness = 1.25f;
		Graphics2D g2=(Graphics2D) g;
		Stroke oldStroke = g2.getStroke();
		g2.setStroke(new BasicStroke(thickness));
		
		g.setColor(Color.black);
		SOMTopology topo = som.getTopo();
		double[] dim = topo.getNeuronDim(w, h, topo.getNeuron(0));
		// desenez pe fiecare
		int nrClusteri = 3;
		int cl[] = new int[nrClusteri]; // cate instante din clusteri sunt
		while (bmus.size() > 0) {
			int r = (int) Math.min(dim[0], dim[1]) / 3;
			BMU bmu = bmus.remove(0);
			int cate = 1;
			int rosii = 0;
			int verzi = 0;
		
			for (int i = 0; i < nrClusteri; i++)
				cl[i] = 0;
			
			String deAfisat=new String();
			
			
			if (bmu.getLabel().toString().startsWith("D")) {
				rosii++;
			} else {
				verzi++;
			}

			cl[(int)bmu.getLabel().toString().charAt(4)-97]++;
			deAfisat=deAfisat+bmu.getLabel().toString().substring(0,4)+" ";
				
			//ptr. structural 
			//deAfisat=deAfisat+bmu.getLabel().toString()+" ";
			
			
			drawCircle(g, w, h, topo, r, bmu, lblAp);
			// drawString(g, w, h, topo, r, bmu, lblAp);
			// daca mai e cazul desenez tot aici cercuri mai mici
			while (bmus.indexOf(bmu) >= 0) {
				// System.out.println("Aici");
				bmu = bmus.remove(bmus.indexOf(bmu));
				cate++;
				if (bmu.getLabel().toString().startsWith("D")) {
					rosii++;
				} else {
					verzi++;
				}
			
				cl[(int)bmu.getLabel().toString().charAt(4)-97]++;
				deAfisat=deAfisat+bmu.getLabel().toString().substring(0,4)+" ";
				
				//ptr structural
				//deAfisat=deAfisat+bmu.getLabel().toString()+" ";
				
				r = r - 2;
				drawCircle(g, w, h, topo, r, bmu, lblAp);
				// drawString(g, w, h, topo, r, bmu, lblAp);
			}
			double[] center = topo.getNeuronCenter(w, h, bmu.getNeuron());
			int xc = (int) (center[0])-nrClusteri*5;
			int yc = (int) (center[1]);
			g.setColor(Color.black);
			// g.drawString("D="+rosii+" N="+verzi, xc, yc);
			String text=new String("(");
			for(int i=0;i<nrClusteri-1;i++)
				text=text+cl[i]+",";
			text=text+cl[nrClusteri-1]+")";
		
			g.drawString(deAfisat, xc-cate*10, yc-40);
		    g.drawString(text, xc, yc);
			
			System.out.println("Nr aparitii bmu>" + cate);
		}
	}

	private static void drawString(Graphics g, int w, int h, SOMTopology topo, int r, BMU bmu,
			LabelApparenceProvider lblAp) {
		if (r < 0) {
			return;
		}
		SOMNeuron n = bmu.getNeuron();
		double[] center = topo.getNeuronCenter(w, h, n);
		// dsenez stringul intr-un anumit loc
		int xc = (int) (center[0] - r);
		int yc = (int) (center[1] - r);
		g.setColor(lblAp.getColor(bmu.getLabel()));
		g.drawString(lblAp.getText(bmu.getLabel()), xc, yc);

	}

	public static void drawData(Graphics g, int w, int h, SOM som, SOMTrainData trD) {

		// optin lista de BMU
		List<BMU> bmus = som.getBMUS(trD);//, som, som.getDistance());
		g.setColor(Color.black);
		SOMTopology topo = som.getTopo();
		double[] dim = topo.getNeuronDim(w, h, topo.getNeuron(0));
		// desenez pe fiecare
		while (bmus.size() > 0) {
			int r = (int) Math.min(dim[0], dim[1]) / 3;
			BMU bmu = bmus.remove(0);
			drawCircle(g, w, h, topo, r, bmu, null);
			// daca mai e cazul desenez tot aici cercuri mai mici

			while (bmus.indexOf(bmu) >= 0) {
				bmu = bmus.remove(bmus.indexOf(bmu));
				r = r - 1;
				drawCircle(g, w, h, topo, r, bmu, null);
			}
		}
	}

	private static void drawCircle(Graphics g, int w, int h, SOMTopology topo, int r, BMU bmu,
			LabelApparenceProvider lblAp) {
		if (r < 0) {
			return;
		}
		SOMNeuron n = bmu.getNeuron();
		double[] center = topo.getNeuronCenter(w, h, n);
		// desenez cercuri
		int xc = (int) (center[0] - r);
		int yc = (int) (center[1] - r);
		if (lblAp != null) {
			g.setColor(lblAp.getColor(bmu.getLabel()));
		}
		g.drawOval(xc, yc, 2 * r, 2 * r);
	}
}
